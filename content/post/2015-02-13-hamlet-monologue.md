---
title: Week 2
subtitle: ... 
date: 04-09-2017
---

We begonnen onze dag wat later omdat veel mensen het lokaal niet konden vinden. Daarna gingen wij als team werken aan Design Challenge one. 

Na het inlichten van een aantal teamleden die niet aanwezig waren over de opdracht, begonnen wij met het vaststellen van de doelgroep. Uiteindelijk werd de **doelgroep CMD'ers** _die niet veel ervaring met de Hogeschool/Rotterdam hebben_, gekozen. We kwamen hierop omdat ikzelf bijna geen ervaring heb met de Hogeschool, omdat ik namelijk van de middelbare afkom en niet zo vaak in Rotterdam ben geweest.  _(en het is aardig wat wennen, serieus.)_

Om onderzoek te doen hebben we daarna een enquete op **SurveyMonkey** gemaakt om achter de mensen hun interesses _(met als onderwerp games)_ te komen. Ik heb hierbij een klein beetje met de vragen geholpen, maar was ondertussen bezig met het globaal bedenken van typen games. Dit om te beslissen wat CMD'ers leuk vinden en wat voor game zij zouden spelen. 

1. Zeeslag
2. Match&Chat - hierbij kan je vrienden maken met mensen in Rotterdam die eerstejaars CMD'ers zijn. Daarbij zijn er ook nog een aantal minigames bij betrokken, maar dit idee is nog niet uitgewerkt.
3. Veroveren van typische plaatsen in Rotterdam.
**En nog wat andere ideeën die voortvloeiden uit wat brainstormen.**

Ook hebben we een aantal ideeën globaal opgeschreven, zoals Zeeslag, een Match&Chat principe, het veroveren van typische plaatsjes in Rotterdam, en nog wat anderen. (Zie dummy voor dit) Hierna zullen wij onderzoek gaan doen. 

Dana heeft een logo gemaakt voor ons team. (voeg deze later toe) Het logo is oranje met wit, en de vijf stralen in het midden staan voor alle teamleden. Hij heeft dit met Illustrator gemaakt. 

Het team is wat afgeleid maar uiteindelijk lukt het wel om met elkaar te werken. Er is een planning gemaakt voor woensdag.
Drie personen gaan games/ideeen verzinnen, en drie personen gaan zich op de doelgroep focussen.  

Aan het einde van de dag hebben we een poster gemaakt.
